<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP" sourcelanguage="en_US">
<context>
    <name>BaseButton</name>
    <message>
        <location filename="../base/BaseButton.qml" line="8"/>
        <source>ok</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../Help.qml" line="49"/>
        <source>Your task is to transfer discs of different sizesfrom the left tower to any other free space.You can only transfer the top drive of the tower.You can not transfer several disks at once and put larger disks on small disks.</source>
        <translation variants="yes">
            <lengthvariant>あなたの仕事は、左のタワーから別の空きスペースに異なるサイズのディスクを転送することです。あなたはタワーの一番上のドライブを移すことができます。一度に複数のディスクを移したり、大きなディスクを小さなディスクに入れることはできません。</lengthvariant>
            <lengthvariant>あなたの仕事は、左のタワーから別の空きスペースに異なるサイズのディスクを転送することです。あなたはタワーの一番上のドライブを移すことができます。一度に複数のディスクを移したり、大きなディスクを小さなディスクに入れることはできません。</lengthvariant>
            <lengthvariant>あなたの仕事は、左のタワーから別の空きスペースに異なるサイズのディスクを転送することです。あなたはタワーの一番上のドライブを移すことができます。一度に複数のディスクを移したり、大きなディスクを小さなディスクに入れることはできません。</lengthvariant>
        </translation>
    </message>
    <message>
        <location filename="../Help.qml" line="62"/>
        <source>Do not show again:</source>
        <translation>再び表示しない：</translation>
    </message>
    <message>
        <location filename="../Help.qml" line="90"/>
        <source>Ok</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <location filename="../about.qml" line="14"/>
        <source>about</source>
        <translation>約</translation>
    </message>
    <message>
        <location filename="../about.qml" line="37"/>
        <source>Product of QuasarApp
* Developers:
* Programmer: Yankovich N. Andrei.
* This game is distributed under the LGPLv3 license.
* Contact: https://github.com/EndrII
* Copyright (C) 2018 Yankovich N. Andrei.</source>
        <translation>QuasarAppの製品
*開発者：
*プログラマー：Yankovich N. Andrei。
*このゲームはLGPLv3ライセンスの下で配布されています。
*お問い合わせ先：https://github.com/EndrII
* Copyright（C）2018 Yankovich N. Andrei。</translation>
    </message>
</context>
<context>
    <name>game</name>
    <message>
        <location filename="../game.qml" line="26"/>
        <source>Start</source>
        <translation>スタート</translation>
    </message>
    <message>
        <location filename="../game.qml" line="37"/>
        <source>Exit</source>
        <translation>出口</translation>
    </message>
    <message>
        <location filename="../game.qml" line="48"/>
        <source>About</source>
        <translation>約</translation>
    </message>
    <message>
        <location filename="../game.qml" line="67"/>
        <source>Tower height:</source>
        <translation>タワーの高さ：</translation>
    </message>
    <message>
        <location filename="../game.qml" line="192"/>
        <source>You have passed the level in %0 steps and unlocked level %1
 Minimum steps for this lvl: %2</source>
        <translation>あなたは %0 ステップでレベルを通過し、レベル %1 をロック解除しました
  このlvlの最小手順：%2</translation>
    </message>
    <message>
        <location filename="../game.qml" line="199"/>
        <source>You have passed the level in %0 steps.
Minimum steps for this lvl: %1</source>
        <translation>%0ステップでレベルを通過しました。
このlvlの最小手順：%1</translation>
    </message>
    <message>
        <location filename="../game.qml" line="301"/>
        <source>Ok</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="9"/>
        <source>Hanoi Towers</source>
        <translation>ハノイタワーズ</translation>
    </message>
</context>
</TS>
