<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
<context>
    <name>BaseButton</name>
    <message>
        <location filename="../base/BaseButton.qml" line="8"/>
        <source>ok</source>
        <translation>ok</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../Help.qml" line="49"/>
        <source>Your task is to transfer discs of different sizesfrom the left tower to any other free space.You can only transfer the top drive of the tower.You can not transfer several disks at once and put larger disks on small disks.</source>
        <translation>Ваше задание состоит в том, чтобы, перенести диски разных размеров с левой башни в любу другую свободную. Вы можете переносить только верхней диск  башни. Нельзя переносить сразу несколько дисков и ставить большее диски на маленькие.</translation>
    </message>
    <message>
        <location filename="../Help.qml" line="62"/>
        <source>Do not show again:</source>
        <translation>Больше не показвать:</translation>
    </message>
    <message>
        <location filename="../Help.qml" line="90"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <location filename="../about.qml" line="14"/>
        <source>about</source>
        <translation>Об Авторе</translation>
    </message>
    <message>
        <location filename="../about.qml" line="37"/>
        <source>Product of QuasarApp
* Developers:
* Programmer: Yankovich N. Andrei.
* This game is distributed under the LGPLv3 license.
* Contact: https://github.com/EndrII
* Copyright (C) 2018 Yankovich N. Andrei.</source>
        <translation>Продукт QuasarApp
* Разработчики:
* Программист: Янкович А. Н. 
* Эта игра распостроняеться в соотвецтвии с лицензией LGPLv3.
* Сайт игры: https://quasarapp.github.io/Hanoi-Towers/
* Copyright (C) 2018 Янкович А. Н.</translation>
    </message>
</context>
<context>
    <name>game</name>
    <message>
        <location filename="../game.qml" line="26"/>
        <source>Start</source>
        <translation>Начать</translation>
    </message>
    <message>
        <location filename="../game.qml" line="37"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../game.qml" line="48"/>
        <source>About</source>
        <translation>об Авторах</translation>
    </message>
    <message>
        <location filename="../game.qml" line="67"/>
        <source>Tower height:</source>
        <translation>Высота башни:</translation>
    </message>
    <message>
        <location filename="../game.qml" line="192"/>
        <source>You have passed the level in %0 steps and unlocked level %1
 Minimum steps for this lvl: %2</source>
        <translation>Вы прошли этот уровень за %0 шагов и разблокировали уровень %1
 Минимум шагов на этом уровне: %2</translation>
    </message>
    <message>
        <location filename="../game.qml" line="199"/>
        <source>You have passed the level in %0 steps.
Minimum steps for this lvl: %1</source>
        <translation>Вы прошли этот уровень за %0 шагов
 Минимум шагов на этом уровне: %1</translation>
    </message>
    <message>
        <location filename="../game.qml" line="301"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="9"/>
        <source>Hanoi Towers</source>
        <translation>Ханойские Башни</translation>
    </message>
</context>
</TS>
