<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>BaseButton</name>
    <message>
        <location filename="../base/BaseButton.qml" line="8"/>
        <source>ok</source>
        <translation>ok</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../Help.qml" line="49"/>
        <source>Your task is to transfer discs of different sizesfrom the left tower to any other free space.You can only transfer the top drive of the tower.You can not transfer several disks at once and put larger disks on small disks.</source>
        <translation>Göreviniz, sol kuleden farklı boyuttaki diskleri başka bir boş alana aktarmaktır. Kulenin sadece üst sürücüsünü aktarabilirsiniz. Birkaç diski aynı anda transfer edemez ve daha küçük disklere daha büyük diskler yerleştiremezsiniz.</translation>
    </message>
    <message>
        <location filename="../Help.qml" line="62"/>
        <source>Do not show again:</source>
        <translation>Tekrar gösterme:</translation>
    </message>
    <message>
        <location filename="../Help.qml" line="90"/>
        <source>Ok</source>
        <translation>ok</translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <location filename="../about.qml" line="14"/>
        <source>about</source>
        <translation>hakkında</translation>
    </message>
    <message>
        <location filename="../about.qml" line="37"/>
        <source>Product of QuasarApp
* Developers:
* Programmer: Yankovich N. Andrei.
* This game is distributed under the LGPLv3 license.
* Contact: https://github.com/EndrII
* Copyright (C) 2018 Yankovich N. Andrei.</source>
        <translation>QuasarApp ürünü
* Geliştiriciler:
* Programcı: Yankovich N. Andrei.
* Bu oyun LGPLv3 lisansı altında dağıtılmaktadır.
* İletişim: https://github.com/EndrII
* Telif Hakkı (C) 2018 Yankovich N. Andrei.</translation>
    </message>
</context>
<context>
    <name>game</name>
    <message>
        <location filename="../game.qml" line="26"/>
        <source>Start</source>
        <translation>başla</translation>
    </message>
    <message>
        <location filename="../game.qml" line="37"/>
        <source>Exit</source>
        <translation>çıkış</translation>
    </message>
    <message>
        <location filename="../game.qml" line="48"/>
        <source>About</source>
        <translation>hakkında</translation>
    </message>
    <message>
        <location filename="../game.qml" line="67"/>
        <source>Tower height:</source>
        <translation>Kule yüksekliği:</translation>
    </message>
    <message>
        <location filename="../game.qml" line="192"/>
        <source>You have passed the level in %0 steps and unlocked level %1
 Minimum steps for this lvl: %2</source>
        <translation>Seviyeyi %0 adımından geçirdiniz ve %1 kilidini açtınız
  Bu lvl için minimum adımlar: %2</translation>
    </message>
    <message>
        <location filename="../game.qml" line="199"/>
        <source>You have passed the level in %0 steps.
Minimum steps for this lvl: %1</source>
        <translation>Seviyeyi %0 adımından geçtiniz.
Bu lvl için minimum adımlar:%1</translation>
    </message>
    <message>
        <location filename="../game.qml" line="301"/>
        <source>Ok</source>
        <translation>ok</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="9"/>
        <source>Hanoi Towers</source>
        <translation>Hanoi Kuleleri</translation>
    </message>
</context>
</TS>
